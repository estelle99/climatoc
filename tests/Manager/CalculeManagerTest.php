<?php

namespace App\tests\Manager; 

use App\Manager\CalculeManager;
use PHPUnit\Framework\TestCase;

class CalculeManagerTest extends TestCase
{

    public function testCalcule(){

        $calculeManager = new CalculeManager();

        $result = $calculeManager->calcule(2,3);
        $this->assertEquals(5, $result);

    }    
    
}

?>