<?php
namespace App\tests\Repository;

use App\Entity\Article;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ArticleRepositoryTest extends KernelTestCase 
{
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp():void {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
             ->get('doctrine')
             ->getManager();
    }

    public function testCountArticle() {
        $article = $this->entityManager
                ->getRepository(Article::class)
                ->findAll();
        $this->assertCount(2, $article);
        
    }
}

?>