<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\SiteRepository;
use App\Service\MailContactService;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{   
    /**
     * Page contact
     *
     * @param ObjectManager $manager
     * @param Request $request
     * @param MailContactService $mail
     * @return Response
     * 
     * @Route("/contact", name="contact")
     */
    public function formContact(SiteRepository $siteRepo, ObjectManager $manager, Request $request, MailContactService $mail) 
    {
        // SELECT
        $site = $siteRepo->findByActiveSite(true);
        $contact = new Contact(); 
         
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);
        $mail->sendEmail($contact);
        

        return $this->render("contact/contact.html.twig", [
            'form' => $form->createView(),
            'site' => $site
        ]);
    }
}
