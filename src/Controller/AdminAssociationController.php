<?php

namespace App\Controller;

use App\Entity\Domain;
use App\Entity\Address;
use App\Form\DomainType;
use App\Form\AddressType;
use App\Entity\Association;
use App\Form\AssociationType;
use App\Repository\DomainRepository;
use App\Repository\AddressRepository;
use App\Repository\CountryRepository;
use App\Repository\AssociationRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted; 
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminAssociationController extends AbstractController
{   
    /**
     * Function for add an new address, domain and association
     *
     * @param Association $association
     * @param DomainRepository $domainRepo
     * @param CountryRepository $countryRepo
     * @param AddressRepository $addressRepo
     * @param ObjectManager $manager
     * @param Request $request
     * @return Response
     * 
     * @Route("/admin/association", name="associationAdmin") 
     * @Route("/admin/association/{id}/updateAsso", name="assoUpdate")
     * @IsGranted("ROLE_ADMIN")
     */
    public function associationAdmin(Association $association= null, DomainRepository $domainRepo, CountryRepository $countryRepo, AddressRepository $addressRepo, ObjectManager $manager, Request $request )
    {
        $newAssociation = $association;
        if(!$newAssociation){
            $newAssociation = new Association();
        }
        
        // CREATE FORM 
        // Association
        $formAssociation = $this->createForm(AssociationType::class, $newAssociation);
        $formAssociation->handleRequest($request);
        $user= $this->getUser();

        // Address
        $newAddress = new Address();
        $formAddress = $this->createForm(AddressType::class, $newAddress);
        $formAddress->handleRequest($request);

        // Domain
        $newDomain = new Domain();
        $formDomain = $this->createForm(DomainType::class, $newDomain);
        $formDomain->handleRequest($request);
        
        // VERIFY THE FORM FOR ADDRESS
        if($formAddress->isSubmitted() && $formAddress->isValid()) {
            // vérifier que le submit n existe pas déjà
            $manager->persist($newAddress);
            $manager->flush();
        
            $this->addFlash(
                'success',
                'Vous avez bien ajouté une nouvelle adresse !'
            );
            return $this->redirectToRoute('associationAdmin');
            

        // VERIFY FORM FOR ASSOCIATION
        } else if ($formAssociation->isSubmitted() && $formAssociation->isValid()) {

            $newAssociation->setUser($user);
            $manager->persist($newAssociation);
            $manager->flush(); 

            $this->addFlash(
                'success',
                'Vous avez bien ajouté une nouvelle association !'
            );

            return $this->redirectToRoute('association');
        
        // VERIFY FORM FOR DOMAIN
        } else if ($formDomain->isSubmitted() && $formDomain->isValid()) {

            // vérifier que le submit n existe pas déjà
            $manager->persist($newDomain);
            $manager->flush(); 

            $this->addFlash(
                'success',
                'Vous avez bien ajouté un nouveau domaine !'
            );

            return $this->redirectToRoute('associationAdmin');
        }

        return $this->render('association/associationAdmin.html.twig', [
            'formAssociation' => $formAssociation->createView(),
            'formAddress' => $formAddress->createView(),
            'formDomain' => $formDomain->createView(),
            'newAssociation' => $newAssociation
        ]);
    }

    /**
     * Function for delete an association
     *
     * @param int $id
     * @param AssociationRepository $assoRepo
     * @param ObjectManager $manager
     * @param Request $request
     * @return Response
     * 
     * @Route("/admin/association/{id}/delete", name="assoDelete")
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAsso($id, AssociationRepository $assoRepo, ObjectManager $manager, Request $request) 
    {
        // SELECT
        $oneAsso = $assoRepo->findOneById($id);

        $oneAsso->setActiveAssociation(false);
        $manager->flush();

        return $this->redirectToRoute('association');
    }
}
