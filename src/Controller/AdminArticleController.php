<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use App\Repository\MessageRepository;
use App\Repository\CategoryArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminArticleController extends AbstractController
{   
    /**
     * Function for add an article
     *
     * @param ObjectManager $manager
     * @param Request $request
     * @return Response
     * 
     * @Route("/admin/addArticle", name="addArticleAdmin")
     * @IsGranted("ROLE_ADMIN")
     */
    public function addArticle(ObjectManager $manager, Request $request) 
    {      
        $newArticle = new Article();
        $formArticle = $this->createForm(ArticleType::class, $newArticle);
        $formArticle->handleRequest($request);

        if($formArticle->isSubmitted() && $formArticle->isValid()) 
        {            
            $manager->persist($newArticle);
            $manager->flush();
            
            $this->addFlash(
                'success',
                'Votre article a bien été enregistré !'
            );

            return $this->redirectToRoute('article');            
        } 

        return $this->render('article/articleAdd.html.twig', [
            'formArticle' => $formArticle->createView()
        ]);
    }

    /**
     * Function for delete an article
     *
     * @param int $id
     * @param ArticleRepository $articleRepo
     * @param ObjectManager $manager
     * @param Request $request
     * @return Response
     * 
     * @Route("/admin/article/{id}/deleteArticle", name="articleDelete")
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteArticle($id, ArticleRepository $articleRepo, ObjectManager $manager, Request $request) 
    {
        // SELECT
        $oneArticle = $articleRepo->findOneById($id);

        $oneArticle->setActiveArticle(false);
        $manager->flush();

        return $this->redirectToRoute('article');
    }

    /**
     * Function for update an article
     *
     * @param int $id
     * @param ArticleRepository $articleRepo
     * @param ObjectManager $manager
     * @param Request $request
     * @return Response
     * 
     * @Route("/admin/article/{id}/updateArticle", name="articleUpdate")
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateArticle($id, ArticleRepository $articleRepo, ObjectManager $manager, Request $request) 
    {
        // SELECT
        $article = $articleRepo->findOneById($id);
        
        // FORM
        $formArticle = $this->createForm(ArticleType::class, $article);
        $formArticle->handleRequest($request);

        if ($formArticle->isSubmitted() && $formArticle->isValid()) {

            $manager->persist($article);
            $manager->flush();

            return $this->redirectToRoute('show', ['id'=> $article->getId()]);
        }        

        return $this->render('article/articleUpdate.html.twig', [
            'article' => $article,  
            'formArticle' => $formArticle->createView()
        ]);
    }

}
