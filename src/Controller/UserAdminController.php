<?php

namespace App\Controller;

use App\Entity\PasswordUpdate;
use App\Form\PasswordUpdateType;
use App\Repository\ActionRepository;
use App\Repository\UserActionRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserAdminController extends AbstractController
{   
    /**
     * Page update the password
     *
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @param ObjectManager $manager
     * @return Response
     * 
     * Update the password
     * @Route("/account/update", name="account-password")
     * 
     * Require ROLE_USER for this method of controller.
     * @IsGranted("ROLE_USER")
     */
    public function updatePassword(Request $request, UserPasswordEncoderInterface $encoder, ObjectManager $manager) {
        // initialise passwordUpdate class from PasswordUpdate.php
        $passwordUpdate = new PasswordUpdate();

        $user = $this->getUser();

        $form = $this->createForm(PasswordUpdateType::class, $passwordUpdate);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            // SELECT
            // get the new password
            $newPassword = $passwordUpdate->getNewPassword(); 

            // encode password 
            $password = $encoder->encodePassword($user, $newPassword);

            // INSERT
            // set new password
            $user->setPassword($password);

            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('clima');
        }

        return $this->render("account/updatePassword.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * Function for update the column of action
     *
     * @param int $id
     * @param ActionRepository $actionRepo
     * @param ObjectManager $manager
     * @param Request $request
     * @param UserActionRepository $userActionRepo
     * @return Response
     * 
     * @Route("/action/{id}/update1", name="updateColumn1")
     */
    public function updateColumn1($id, ActionRepository $actionRepo, ObjectManager $manager, Request $request, UserActionRepository $userActionRepo) 
    {
        // SELECT
        $user = $this->getUser();
        $action = $userActionRepo->findOneBy(['user' => $user,'action' =>$id]);
        // INSERT
        $action->setColumnUserAction(2);

        $manager->flush();

        return $this->redirectToRoute('action');
    }

    /**
     * Function for update the column of action
     *
     * @param int $id
     * @param ActionRepository $actionRepo
     * @param ObjectManager $manager
     * @param Request $request
     * @param UserActionRepository $userActionRepo
     * @return Response
     * 
     * @Route("/action/{id}/update2", name="updateColumn2")
     */
    public function updateColumn2($id, ActionRepository $actionRepo, ObjectManager $manager, Request $request, UserActionRepository $userActionRepo) 
    {
        // SELECT
        $user = $this->getUser();
        $action = $userActionRepo->findOneBy(['user' => $user,'action' =>$id]);
        // INSERT
        $action->setColumnUserAction(1);

        $manager->flush();

        return $this->redirectToRoute('action');
    }

    /**
     * Function for valid an action
     *
     * @param int $id
     * @param ActionRepository $actionRepo
     * @param ObjectManager $manager
     * @param Request $request
     * @param UserActionRepository $userActionRepo
     * @return Response
     * 
     * @Route("/action/{id}/valid", name="validColumn2")
     */
    public function validColumn2($id, ActionRepository $actionRepo, ObjectManager $manager, Request $request, UserActionRepository $userActionRepo) 
    {
        // SELECT
        $user = $this->getUser();
        $action = $userActionRepo->findOneBy(['user' => $user,'action' =>$id]);
        // INSERT
        $action->setColumnUserAction(0);

        $manager->flush();

        return $this->redirectToRoute('action');
    }

    /**
     * Function for delete an action
     *
     * @param int $id
     * @param ActionRepository $actionRepo
     * @param ObjectManager $manager
     * @param Request $request
     * @param UserActionRepository $userActionRepo
     * @return Response
     * 
     * @Route("/action/{id}/delete", name="deleteColumn0")
     */
    public function deleteColumn0($id, ActionRepository $actionRepo, ObjectManager $manager, Request $request, UserActionRepository $userActionRepo) 
    {
        // SELECT
        $user = $this->getUser();
        $action = $userActionRepo->findOneBy(['user' => $user,'action' =>$id]);
        // INSERT
        $action->setColumnUserAction(1);

        $manager->flush();

        return $this->redirectToRoute('action');
    }

}
