<?php

namespace App\Controller;

use App\Entity\Address;
use App\Form\AddressType;
use App\Repository\AddressRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminAddressController extends AbstractController
{   
    /**
     * @Route("/admin/address", name="addressAdd")
     * @Route("/admin/address/{id}/updateAddress", name="addressUpdate")
     * @IsGranted("ROLE_ADMIN")
     */
    public function addressAdd(Address $address= null, Request $request, ObjectManager $manager, AddressRepository $addressRepo) 
    {
        $compt = 0;
        $newAddress = $address;
        if(!$newAddress){
            $newAddress = new Address();
            $compt = 1;
        }

        // SELECT
        $address = $addressRepo->findByActiveAddress(true);

        $formAddress = $this->createForm(AddressType::class, $newAddress);
        $formAddress->handleRequest($request);
        
        if($formAddress->isSubmitted() && $formAddress->isValid()) {
            // vérifier que le submit n existe pas déjà
            $manager->persist($newAddress);
            $manager->flush();
           
            if($compt){
                $this->addFlash(
                    'success',
                    'Vous avez bien ajouté une nouvelle adresse !'
                );
            } else {
                $this->addFlash(
                    'success',
                    'Vous avez bien modifié une adresse !'
                );
            }
            return $this->redirectToRoute("addressAdd");
        }
        return $this->render("association/address/address.html.twig", [
            'formAddress' => $formAddress->createView(),
            'address' => $address,
            'compt' => $compt
        ]);
    }

    /**
     * @Route("/admin/address/{id}/deleteAddress", name="addressDelete")
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAddress(Address $address, AddressRepository $addressRepo, ObjectManager $manager, Request $request) 
    {
        $address->setActiveAddress(false);
        $manager->flush();

        return $this->redirectToRoute("addressAdd");
    }
}
