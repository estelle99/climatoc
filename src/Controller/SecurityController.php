<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserAction;
use App\Entity\PasswordUpdate;
use App\Form\RegistrationType;
use App\Repository\RoleRepository;
use App\Repository\UserRepository;
use App\Repository\ActionRepository;
use App\Service\MailPasswordService;
use App\Repository\UserActionRepository;
use App\Form\PasswordUpdateNotConnectedType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * Page inscription
     *
     * @param ActionRepository $actionRepo
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param RoleRepository $roleRepo
     * @param ObjectManager $manager
     * @param Request $request
     * @return Response
     * 
     * @Route("/inscription", name="registration")
     */
    public function registration(ActionRepository $actionRepo, UserPasswordEncoderInterface $passwordEncoder, RoleRepository $roleRepo, ObjectManager $manager, Request $request) 
    {
        // SELECT
        // For display list of role
        $role = $roleRepo->findAll();
        $actions = $actionRepo->findAll();

        $user = new User();

        // form of registration       
        $form = $this->createForm(RegistrationType::class, $user);
  
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            
            //Encode the password
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());

            // INSERT
            $user->setPassword($password)->setIsActive(true);

            //Save the User!
            $manager->persist($user);

            // Associate a user with an action
            foreach ($actions as $action) {
                $userAction = new UserAction();
                $userAction->setUser($user)
                           ->setAction($action)
                           ->setColumnUserAction(1);
                $manager->persist($userAction);
            }
            $manager->flush();           
            
            return $this->redirectToRoute('login');
        } 
        
        return $this->render('login/registration.html.twig', [
            'form' => $form->createView(),
            'role' => $role
        ]); 
    }

    /**
     * Function and page for login
     *
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     * 
     * @Route("/connexion", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('login/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]); 
    }

    /**
     * Function for logout
     *
     * @return void
     * 
     * @Route("/deconnexion", name="logout")
     */
    public function logout() {}

    /**
     * Function for forgot password
     *
     * @return Response
     * 
     * @Route("/connexion/forgotPassword", name="forgotPassword")
     */
    public function forgotPassword() 
    {
        return $this->render('account/forgotPassword.html.twig');
    }

    /**
     * Function for reload password
     *
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserRepository $userRepo
     * @param MailPasswordService $mail
     * @return Response
     * 
     * @Route("/connexion/forgotPasswordUpdate", name="forgotPasswordUpdate")
     */
    public function forgotPasswordSubmit(Request $request, ObjectManager $manager, UserRepository $userRepo, MailPasswordService $mail ) 
    {
        // SELECT
            // email in the "name" of the twig page : forgotPassword.html.twig
            $mailUser = $request->request->get('email');
            // the user who owns the email
            $userEmail = $userRepo->findBy(['email'=> $mailUser, 'isActive'=>1]);

            if($userEmail) {
                $mail->sendEmail($userEmail);
                return $this->render('account/confirmationEmailPassword.html.twig');
            } else {
                $this->addFlash(
                    'danger',
                    'Votre adresse mail n\'existe pas chez nous. Veuillez vous inscrire !'
                );
            }
            
            return $this->render('account/forgotPassword.html.twig');
    }

    /**
     * Function for reload the password
     *
     * @param string $token
     * @param UserRepository $userRepo
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param ObjectManager $manager
     * @param Request $request
     * @return Response
     * 
     * @Route("/connexion/{token}", name="reloadPassword")
     */
    public function reloadPassword($token, UserRepository $userRepo, UserPasswordEncoderInterface $passwordEncoder, ObjectManager $manager, Request $request) 
    {   
        // SELECT
            $token = $userRepo->findOneByTokenUser($token);
           
        if($token) {
            $passwordUpdate = new PasswordUpdate();
            $form = $this->createForm(PasswordUpdateNotConnectedType::class, $passwordUpdate);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()) { 
                // SELECT
                $email = $request->request->get("email");
                $emailUser = $userRepo->findBy(['email'=> $email, 'isActive' =>1]);
                
        
                if($emailUser) {
                    // get the new password
                    $newPassword = $passwordUpdate->getNewPassword();   
                    // encode password 
                    $password = $passwordEncoder->encodePassword($emailUser[0], $newPassword);
                    // INSERT
                        // set new password
                        $emailUser[0]->setPassword($password)
                                    ->setTokenUser("");
                
                    $manager->persist($emailUser[0]);
                    $manager->flush();

                    return $this->redirectToRoute('login');
                } else {
                    $this->addFlash(
                        'danger',
                        'Votre adresse mail n\'existe pas chez nous. Veuillez vous inscrire !'
                    );
                }
            } 
            return $this->render('account/reloadPassword.html.twig', [
                'form' => $form->createView()
            ]);

        } 
        
        throw $this->createNotFoundException();
    }
}
