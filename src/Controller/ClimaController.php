<?php

namespace App\Controller;

use App\Form\UserType;
use App\Entity\Message;
use App\Form\MessageType;
use App\Repository\UserRepository;
use App\Repository\ActionRepository;
use App\Repository\ArticleRepository;
use App\Repository\MessageRepository;
use App\Repository\UserActionRepository;
use App\Repository\AssociationRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ClimaController extends AbstractController
{
    /**
     * Home page
     *
     * @param Request $request
     * @param ObjectManager $manager
     * @param ArticleRepository $articleRepo
     * @return Response
     * 
     * @Route("/", name="clima")
     */
    public function index(Request $request, ObjectManager $manager, ArticleRepository $articleRepo)
    {
        $article = $articleRepo->findByActiveArticle('1');
        return $this->render('clima/index.html.twig', [
            'controller_name' => 'Bien le bonjour ',
            'video' => 'https://www.youtube.com/embed/RIQqVqQs9Xs',
            'actu' => 'Les actualités',
            'titreArticle' => 'Nos derniers articles',
            'article' => $article
        ]);
    }

    /**
     * Page Article
     *
     * @param ArticleRepository $articleRepo
     * @return Response
     * 
     * @Route("/article", name="article")
     */
    public function article(ArticleRepository $articleRepo)
    {
        $article = $articleRepo->findByActiveArticle('1');

        return $this->render('article/article.html.twig', [
            'article' => $article,
        ]);
    }

    /**
     * Page for show an article
     *
     * @param int $id
     * @param ArticleRepository $articleRepo
     * @param MessageRepository $messageRepo
     * @param UserRepository $userRepo
     * @param ObjectManager $manager
     * @param Request $request
     * @return Response
     * 
     * @Route("/article/{id}", name="show", requirements={"id"="\d+"})
     */
    public function show($id, ArticleRepository $articleRepo, MessageRepository $messageRepo, UserRepository $userRepo, ObjectManager $manager, Request $request) 
    {
        // SELECT
        $idArticle = $articleRepo->findOneBy(["id"=>$id,"activeArticle"=>1]);
        $message = $messageRepo->findBy(["article"=>$id,"statusMessage"=>Message::VALIDE, "activeMessage"=>1]);
        $messageFalse = $messageRepo->findBy(["article"=>$id,"statusMessage"=>Message::EN_COURS, "activeMessage"=>1]);
        // Active article
        $article = $articleRepo->findByActiveArticle('1');
        // Management of error
        if(!$idArticle) {
            throw $this->createNotFoundException('The product does not exist');
        }
        
        // FORM OF MESSAGE
        // I instantiated "Message"  
        $newMessage = new Message();

        $user = $this->getUser();
        
        $form = $this->createForm(MessageType::Class, $newMessage);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {

            $newMessage->setUser($user)
                       ->setArticle($idArticle)
                       ->setActiveMessage(true)
                       ->setStatusMessage(Message::EN_COURS);

            $manager->persist($newMessage);
            $manager->flush();

            return $this->redirectToRoute('message');
        }

        return $this->render('article/articleShow.html.twig', [
            'form' => $form->createView(),
            'messageFalse' => $messageFalse,
            'newMessage' => $newMessage,
            'message' => $message,
            'idArticle' => $idArticle,
            'article' => $article
        ]); 
    }

    /**
     * Page action
     *
     * @param UserActionRepository $userActionRepo
     * @param ActionRepository $actionRepo
     * @return Response
     * 
     * @Route("/action", name="action")
     */
    public function action(UserActionRepository $userActionRepo, ActionRepository $actionRepo)
    {
        $user = $this->getUser();
        if( $user != null) {
            $idUser = $user;
           
            $action1 = $userActionRepo->findBy(['user' => $user->id, 'columnUserAction' =>1]);
            $action2 = $userActionRepo->findBy(['user' => $user->id, 'columnUserAction' =>2]);
            $action3 = $userActionRepo->findBy(['user' => $user->id, 'columnUserAction' =>0]);

            return $this->render('action/action.html.twig',[
                'action1' => $action1,
                'action2' => $action2,
                'action3' => $action3
            ]);
        } else {
            $action = $actionRepo->findByActiveAction(1);

            return $this->render('action/action.html.twig',[
                'actions' => $action
            ]);
        }
    }

    /**
     * Page association
     *
     * @param AssociationRepository $associaitonRepo
     * @return Response
     * 
     * @Route("/association", name="association")
     */
    public function association(AssociationRepository $associaitonRepo)
    {
        $association = $associaitonRepo->findByActiveAssociation('1');

        return $this->render('association/association.html.twig', [
            'association'=> $association
        ]);
    }

    /**
     * Page account of user
     *
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserRepository $userRepo
     * @return Response
     * 
     * @Route("/account", name="account")
     * 
     * Require ROLE_USER for this method of controller.
     * @IsGranted("ROLE_USER")
     */
    public function account(Request $request, ObjectManager $manager, UserRepository $userRepo) 
    {
        // SELECT
        $thisUser = $this->getUser();

        // FORM
        $formUser = $this->createForm(UserType::class, $thisUser);
        $formUser->handleRequest($request);

        if($formUser->isSubmitted() && $formUser->isValid()) { 
            $manager->persist($formUser);
            $manager->flush();
            return $this->redirectToRoute('account');
        } 
        
        return $this->render('account/account.html.twig', [
            'formUser' => $formUser->createView()
        ]);
    }
}
