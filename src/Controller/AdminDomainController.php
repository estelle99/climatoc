<?php

namespace App\Controller;

use App\Entity\Domain;
use App\Form\DomainType;
use App\Repository\DomainRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminDomainController extends AbstractController
{   
    /**
     * @Route("/admin/domain", name="domainAdd")
     * @Route("/admin/domain/{id}/updateDomain", name="domainUpdate")
     * @IsGranted("ROLE_ADMIN")
     */
    public function domainAdd(Domain $domain= null, Request $request, ObjectManager $manager, DomainRepository $domainRepo) 
    {
        $compt = 0;
        $newDomain = $domain;
        if(!$newDomain){
            $newDomain = new Domain();
            $compt = 1;
        }

        // SELECT
        $domain = $domainRepo->findByActiveDomain(true);

        $formDomain = $this->createForm(DomainType::class, $newDomain);
        $formDomain->handleRequest($request);
        
        if($formDomain->isSubmitted() && $formDomain->isValid()) {
            // vérifier que le submit n existe pas déjà
            $manager->persist($newDomain);
            $manager->flush();
           
            if($compt){
                $this->addFlash(
                    'success',
                    'Vous avez bien ajouté un nouveau domaine !'
                );
            } else {
                $this->addFlash(
                    'success',
                    'Vous avez bien modifié un domaine !'
                );
            }
            return $this->redirectToRoute("domainAdd");
        }
        return $this->render("association/domain/domain.html.twig", [
            'formDomain' => $formDomain->createView(),
            'domain' => $domain
        ]);
    }

    /**
     * @Route("/admin/domain/{id}/deleteDomain", name="domainDelete")
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteDomain(Domain $domain, DomainRepository $domainRepo, ObjectManager $manager, Request $request) 
    {
        $domain->setActiveDomain(false);
        $manager->flush();

        return $this->redirectToRoute("domainAdd");
    }
}
