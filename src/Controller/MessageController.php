<?php

namespace App\Controller;

use App\Entity\Message;
use App\Repository\MessageRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MessageController extends AbstractController
{   
    /**
     * Page message for the users
     *
     * @param MessageRepository $messageRepo
     * @param ObjectManager $manager
     * @param Request $request
     * @return Response
     * 
     * @Route("/message", name="message")
     * 
     * Require ROLE_USER for this method of controller.
     * @IsGranted("ROLE_USER")
     */
    public function message(MessageRepository $messageRepo, ObjectManager $manager, Request $request)
    {
        $user = $this->getUser();
        $message = $messageRepo->findBy(['user'=> $user, 'activeMessage' => 1]);

        return $this->render('message/message.html.twig', [
            'messages' => $message
        ]);
    }

    /**
     * Function for the admin to inactive a message 
     *
     * @param Message $message
     * @param ObjectManager $manager
     * @param Request $request
     * @return Response
     * 
     * @Route("/admin/article/{id}/inactive", name="messageInactive")
     * 
     * Message in article. The Admin regulate messages of user
     * @IsGranted("ROLE_ADMIN")
     */
    public function inactiveMessage(Message $message, ObjectManager $manager, Request $request) 
    {
        // SELECT
        $article = $message->getArticle();
        $idArticle = $article->getId();

        $message->setStatusMessage(Message::PAS_VALIDE)
                ->setActiveMessage(true);
        
        $manager->persist($message);
        $manager->flush();

        return $this->redirectToRoute('show', ['id'=> $article->getId()]);
    }

    /**
     * Function for the admin to valid a message
     *
     * @param Message $message
     * @param ObjectManager $manager
     * @param Request $request
     * @return Response
     * 
     * @Route("/admin/article/{id}/valid", name="messageValid")
     * 
     * Admin valid a message
     * @IsGranted("ROLE_ADMIN")
     */
    public function validMessage(Message $message, ObjectManager $manager, Request $request) 
    {
        // SELECT
        $article = $message->getArticle();
        $idArticle = $article->getId();

        $message->setStatusMessage(Message::VALIDE);
        $manager->flush();

        return $this->redirectToRoute('show', ['id'=> $article->getId()]);
    }

    /**
     * Function for the user to delete his message 
     *
     * @param Message $message
     * @param ObjectManager $manager
     * @param Request $request
     * @return Response
     * 
     * @Route("/message/{id}/delete", name="messageUserDelete")
     * 
     * The user delete her message
     * @Security("is_granted('ROLE_USER') and user == message.getUser()")
     */
    public function deleteUserMessage(Message $message, ObjectManager $manager, Request $request) 
    {
        $message->setActiveMessage(0);
    
        $manager->persist($message);
        $manager->flush();

        return $this->redirectToRoute('message');
    }
}
