<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity("email", message="Vous êtes déjà inscrit")
 */
class User implements UserInterface
{
    private $eraseCredentials;
    private $salt;

    /** 
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Email(
     *     message = "Votre adresse mail n'est pas valide !",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=8, minMessage="Votre mot de passe doit faire au minimum 8 caractères !")
     */
    private $password;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=250)
     * @Assert\Length(min=8, minMessage="Votre mot de passe doit faire au minimum 8 caractères !")
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tokenUser;

    /**
     * @ORM\Column(type="boolean") 
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="user")
     */
    private $message;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserAction", mappedBy="user")
     */
    private $userAction;
    
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Role", mappedBy="roleUser")
     */
    private $userRole;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Association", mappedBy="user")
     */
    private $association;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Site", mappedBy="user")
     */
    private $site;

    public function __construct()
    {
        $this->message = new ArrayCollection();
        $this->userRole = new ArrayCollection();
        $this->userAction = new ArrayCollection();
        $this->association = new ArrayCollection();
        $this->site = new ArrayCollection();
    }
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function setSalt()
    {
        $this->salt = $salt;

        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername(string $username)
    {
        $this->username = $username;

        return $this;
    }

    public function eraseCredentials()
    {
        return $this->eraseCredentials;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getRoles()
    {
        // map permet (de boucler) d'itérer sur un objet 
        $roles = $this->userRole->map(function($role){
            return $role->getTypeRole();
        })->toArray();

        $roles[] = 'ROLE_USER';
        return $roles;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessage(): Collection
    {
        return $this->message;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setUser($this);
        }
 
        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->contains($message)) {
            $this->messages->removeElement($message);
            // set the owning side to null (unless already changed)
            if ($message->getUser() === $this) {
                $message->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Role[]
     */
    public function getUserRole(): Collection
    {
        return $this->userRole;
    }

    public function addUserRole(Role $userRole): self
    {
        if (!$this->userRole->contains($userRole)) {
            $this->userRole[] = $userRole;
            $userRole->addRoleUser($this);
        }

        return $this;
    }

    public function removeUserRole(Role $userRole): self
    {
        if ($this->userRole->contains($userRole)) {
            $this->userRole->removeElement($userRole);
            $userRole->removeRoleUser($this);
        }

        return $this;
    }

    public function getTokenUser(): ?string
    {
        return $this->tokenUser;
    }

    public function setTokenUser(?string $tokenUser): self
    {
        $this->tokenUser = $tokenUser;

        return $this;
    }

    /**
     * @return Collection|UserAction[]
     */
    public function getUserAction(): Collection
    {
        return $this->userAction;
    }

    public function addUserAction(UserAction $userAction): self
    {
        if (!$this->userAction->contains($userAction)) {
            $this->userAction[] = $userAction;
            $userAction->setUser($this);
        }

        return $this;
    }

    public function removeUserAction(UserAction $userAction): self
    {
        if ($this->userAction->contains($userAction)) {
            $this->userAction->removeElement($userAction);
            // set the owning side to null (unless already changed)
            if ($userAction->getUser() === $this) {
                $userAction->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Association[]
     */
    public function getAssociation(): Collection
    {
        return $this->association;
    }

    public function addAssociation(Association $association): self
    {
        if (!$this->association->contains($association)) {
            $this->association[] = $association;
            $association->setUser($this);
        }

        return $this;
    }

    public function removeAssociation(Association $association): self
    {
        if ($this->association->contains($association)) {
            $this->association->removeElement($association);
            // set the owning side to null (unless already changed)
            if ($association->getUser() === $this) {
                $association->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Site[]
     */
    public function getSite(): Collection
    {
        return $this->site;
    }

    public function addSite(Site $site): self
    {
        if (!$this->site->contains($site)) {
            $this->site[] = $site;
            $site->addUser($this);
        }

        return $this;
    }

    public function removeSite(Site $site): self
    {
        if ($this->site->contains($site)) {
            $this->site->removeElement($site);
            $site->removeUser($this);
        }

        return $this;
    }
}
