<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 */
class Article
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Url
     */
    private $picturesArticle;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     * @Assert\Type("string")
     */
    private $textArticle;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $titleArticle;

    /**
     * @ORM\Column(type="boolean")
     */
    private $activeArticle;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CategoryArticle", inversedBy="articles")
     * @Assert\NotBlank
     */
    public $category;

    /**
     * @ORM\PrePersist
     */
    public function addAssociation() 
    {
        $this->setActiveAssociation(true);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getActiveArticle(): ?bool
    {
        return $this->activeArticle;
    }

    public function setActiveArticle(bool $activeArticle): self
    {
        $this->activeArticle = $activeArticle;

        return $this;
    }

    public function getPicturesArticle(): ?string
    {
        return $this->picturesArticle;
    }

    public function setPicturesArticle(string $picturesArticle): self
    {
        $this->picturesArticle = $picturesArticle;

        return $this;
    }

    public function getTextArticle(): ?string
    {
        return $this->textArticle;
    }

    public function setTextArticle(string $textArticle): self
    {
        $this->textArticle = $textArticle;

        return $this;
    }

    public function getTitleArticle(): ?string
    {
        return $this->titleArticle;
    }

    public function setTitleArticle(string $titleArticle): self
    {
        $this->titleArticle = $titleArticle;

        return $this;
    }

    public function getCategory(): ?CategoryArticle
    {
        return $this->category;
    }

    public function setCategory(?CategoryArticle $category): self
    {
        $this->category = $category;

        return $this;
    }
}
