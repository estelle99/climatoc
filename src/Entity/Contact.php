<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

class Contact
{
    private $mailContact;

    private $textContact;

    private $objectContact;

    public function getMailContact(): ?string
    {
        return $this->mailContact;
    }

    public function setMailContact(string $mailContact): self
    {
        $this->mailContact = $mailContact;

        return $this;
    }

    public function getTextContact(): ?string
    {
        return $this->textContact;
    }

    public function setTextContact(string $textContact): self
    {
        $this->textContact = $textContact;

        return $this;
    }

    public function getObjectContact(): ?string
    {
        return $this->objectContact;
    }

    public function setObjectContact(string $objectContact): self
    {
        $this->objectContact = $objectContact;

        return $this;
    }
}
