<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AssociationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Association
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Type("string")
     */
    private $nameAssociation;

    /**
     * @ORM\Column(type="text")
     * @Assert\Type("string")
     */
    private $descriptionAssociation;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Url
     */
    private $logoAssociation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank
     * @Assert\Url
     */
    private $linkAssociation;

    /**
     * @ORM\Column(type="boolean")
     */
    private $activeAssociation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Address", inversedBy="association")
     * @Assert\NotBlank
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain", inversedBy="domain")
     * @Assert\NotBlank
     */
    private $domain;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="association")
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Site", mappedBy="association")
     */
    private $site;

    /**
     * @ORM\PrePersist
     */
    public function addAssociation() 
    {
        $this->setActiveAssociation(true);
    
    }

    public function __construct()
    {
        $this->message = new ArrayCollection();
        $this->site = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameAssociation(): ?string
    {
        return $this->nameAssociation;
    }

    public function setNameAssociation(string $nameAssociation): self
    {
        $this->nameAssociation = $nameAssociation;

        return $this;
    }

    public function getDescriptionAssociation(): ?string
    {
        return $this->descriptionAssociation;
    }

    public function setDescriptionAssociation(string $descriptionAssociation): self
    {
        $this->descriptionAssociation = $descriptionAssociation;

        return $this;
    }

    public function getLogoAssociation(): ?string
    {
        return $this->logoAssociation;
    }

    public function setLogoAssociation(string $logoAssociation): self
    {
        $this->logoAssociation = $logoAssociation;

        return $this;
    }

    public function getActiveAssociation(): ?bool
    {
        return $this->activeAssociation;
    }

    public function setActiveAssociation(bool $activeAssociation): self
    {
        $this->activeAssociation = $activeAssociation;

        return $this;
    }

    public function getAddress(): ?address
    {
        return $this->address;
    }

    public function setAddress(?address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getDomain(): ?domain
    {
        return $this->domain;
    }

    public function setDomain(?domain $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getLinkAssociation(): ?string
    {
        return $this->linkAssociation;
    }

    public function setLinkAssociation(?string $linkAssociation): self
    {
        $this->linkAssociation = $linkAssociation;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Site[]
     */
    public function getSite(): Collection
    {
        return $this->site;
    }

    public function addSite(Site $site): self
    {
        if (!$this->site->contains($site)) {
            $this->site[] = $site;
            $site->addAssociation($this);
        }

        return $this;
    }

    public function removeSite(Site $site): self
    {
        if ($this->site->contains($site)) {
            $this->site->removeElement($site);
            $site->removeAssociation($this);
        }

        return $this;
    }
}
