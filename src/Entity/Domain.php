<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DomainRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Domain
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $nameDomain;

    /**
     * @ORM\Column(type="boolean")
     */
    private $activeDomain;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Association", mappedBy="domain")
     * @Assert\NotBlank
     */
    private $association;

    /**
     * @ORM\PrePersist
     */
    public function addNewDomain() 
    {
        $this->setActiveDomain(true);
    }

    public function __construct()
    {
        $this->association = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameDomain(): ?string
    {
        return $this->nameDomain;
    }

    public function setNameDomain(string $nameDomain): self
    {
        $this->nameDomain = $nameDomain;

        return $this;
    }

    /**
     * @return Collection|Association[]
     */
    public function getAssociation(): Collection
    {
        return $this->association;
    }

    public function addAssociation(Association $association): self
    {
        if (!$this->association->contains($association)) {
            $this->association[] = $association;
            $association->setAssociation($this);
        }

        return $this;
    }

    public function removeAssociation(Association $association): self
    {
        if ($this->association->contains($association)) {
            $this->association->removeElement($association);
            // set the owning side to null (unless already changed)
            if ($association->getAssociation() === $this) {
                $association->setAssociation(null);
            }
        }

        return $this;
    }

    public function getActiveDomain(): ?bool
    {
        return $this->activeDomain;
    }

    public function setActiveDomain(bool $activeDomain): self
    {
        $this->activeDomain = $activeDomain;

        return $this;
    }
}
