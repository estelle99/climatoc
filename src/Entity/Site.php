<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SiteRepository")
 */
class Site
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameSite;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phoneSite;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $emailSite;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $logoSite;

    /**
     * @ORM\Column(type="boolean")
     */
    private $activeSite;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Address", inversedBy="site")
     */
    private $address;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Association", inversedBy="site")
     */
    private $association;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="site")
     */
    private $user;

    public function __construct()
    {
        $this->address = new ArrayCollection();
        $this->association = new ArrayCollection();
        $this->user = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameSite(): ?string
    {
        return $this->nameSite;
    }

    public function setNameSite(string $nameSite): self
    {
        $this->nameSite = $nameSite;

        return $this;
    }

    public function getPhoneSite(): ?string
    {
        return $this->phoneSite;
    }

    public function setPhoneSite(string $phoneSite): self
    {
        $this->phoneSite = $phoneSite;

        return $this;
    }

    public function getEmailSite(): ?string
    {
        return $this->emailSite;
    }

    public function setEmailSite(string $emailSite): self
    {
        $this->emailSite = $emailSite;

        return $this;
    }

    public function getActiveSite(): ?bool
    {
        return $this->activeSite;
    }

    public function setActiveSite(bool $activeSite): self
    {
        $this->activeSite = $activeSite;

        return $this;
    }

    /**
     * @return Collection|Address[]
     */
    public function getAddress(): Collection
    {
        return $this->address;
    }

    public function addAddress(Address $address): self
    {
        if (!$this->address->contains($address)) {
            $this->address[] = $address;
        }

        return $this;
    }

    public function removeAddress(Address $address): self
    {
        if ($this->address->contains($address)) {
            $this->address->removeElement($address);
        }

        return $this;
    }

    /**
     * @return Collection|Association[]
     */
    public function getAssociation(): Collection
    {
        return $this->association;
    }

    public function addAssociation(Association $association): self
    {
        if (!$this->association->contains($association)) {
            $this->association[] = $association;
        }

        return $this;
    }

    public function removeAssociation(Association $association): self
    {
        if ($this->association->contains($association)) {
            $this->association->removeElement($association);
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
        }

        return $this;
    }

    public function getLogoSite(): ?string
    {
        return $this->logoSite;
    }

    public function setLogoSite(string $logoSite): self
    {
        $this->logoSite = $logoSite;

        return $this;
    }
}
