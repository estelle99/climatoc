<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CountryRepository")
 */
class Country
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameCountry;

    /**
     * @ORM\Column(type="boolean")
     */
    private $activeCountry;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Address", mappedBy="country")
     */
    private $address;

    public function __construct()
    {
        $this->address = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameCountry(): ?string
    {
        return $this->nameCountry;
    }

    public function setNameCountry(string $nameCountry): self
    {
        $this->nameCountry = $nameCountry;

        return $this;
    }

    public function getUrlImageCountry(): ?string
    {
        return $this->urlImageCountry;
    }

    public function setUrlImageCountry(string $urlImageCountry): self
    {
        $this->urlImageCountry = $urlImageCountry;

        return $this;
    }

    public function getActiveCountry(): ?bool
    {
        return $this->activeCountry;
    }

    public function setActiveCountry(bool $activeCountry): self
    {
        $this->activeCountry = $activeCountry;

        return $this;
    }

    /**
     * @return Collection|Address[]
     */
    public function getAddress(): Collection
    {
        return $this->address;
    }

    public function addAddress(Address $address): self
    {
        if (!$this->address->contains($address)) {
            $this->address[] = $address;
            $address->setCountry($this);
        }

        return $this;
    }

    public function removeAddress(Address $address): self
    {
        if ($this->address->contains($address)) {
            $this->address->removeElement($address);
            // set the owning side to null (unless already changed)
            if ($address->getCountry() === $this) {
                $address->setCountry(null);
            }
        }

        return $this;
    }
}
