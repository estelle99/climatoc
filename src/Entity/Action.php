<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ActionRepository")
 */
class Action
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $wordingAction;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     * @Assert\Type("string")
     */
    private $descriptionAction;

    /**
     * @ORM\Column(type="boolean")
     */
    private $activeAction;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserAction", mappedBy="action")
     */
    private $userAction;

    public function __construct()
    {
        $this->userAction = new ArrayCollection();
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWordingAction(): ?string
    {
        return $this->wordingAction;
    }

    public function setWordingAction(string $wordingAction): self
    {
        $this->wordingAction = $wordingAction;

        return $this;
    }

    public function getDescriptionAction(): ?string
    {
        return $this->descriptionAction;
    }

    public function setDescriptionAction(string $descriptionAction): self
    {
        $this->descriptionAction = $descriptionAction;

        return $this;
    }

    public function getActiveAction(): ?bool
    {
        return $this->activeAction;
    }

    public function activeAction(bool $activeAction): self
    {
        $this->activeAction = $activeAction;

        return $this;
    }

    /**
     * @return Collection|UserAction[]
     */
    public function getUserAction(): Collection
    {
        return $this->userAction;
    }

    public function addUserAction(UserAction $userAction): self
    {
        if (!$this->userAction->contains($userAction)) {
            $this->userAction[] = $userAction;
            $userAction->setAction($this);
        }

        return $this;
    }

    public function removeUserAction(UserAction $userAction): self
    {
        if ($this->userAction->contains($userAction)) {
            $this->userAction->removeElement($userAction);
            // set the owning side to null (unless already changed)
            if ($userAction->getAction() === $this) {
                $userAction->setAction(null);
            }
        }

        return $this;
    }
}
