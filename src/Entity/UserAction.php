<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserActionRepository")
 */
class UserAction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userAction")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Action", inversedBy="userAction")
     */
    private $action;
    
    /**
     * @ORM\Column(type="decimal")
     */
    private $columnUserAction;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getColumnUserAction()
    {
        return $this->columnUserAction;
    }

    public function setColumnUserAction($columnUserAction): self
    {
        $this->columnUserAction = $columnUserAction;

        return $this;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAction(): ?action
    {
        return $this->action;
    }

    public function setAction(?action $action): self
    {
        $this->action = $action;

        return $this;
    }
}
