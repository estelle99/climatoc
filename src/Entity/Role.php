<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoleRepository")
 */
class Role
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typeRole;

    /**
     * @ORM\Column(type="boolean")
     */
    private $activeRole;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="userRole")
     */
    private $roleUser;

    public function __construct()
    {
        $this->user = new ArrayCollection();
        $this->roles = new ArrayCollection();
        $this->roleUser = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeRole(): ?string
    {

        return $this->typeRole;
    }

    public function setTypeRole(string $typeRole): self
    {
        $this->typeRole = $typeRole;

        return $this;
    }

    public function getActiveRole(): ?bool
    {
        return $this->activeRole;
    }

    public function setActiveRole(bool $activeRole): self
    {
        $this->activeRole = $activeRole;

        return $this;
    }

    /**
     * @return Collection|user[]
     */
    public function getRoleUser(): Collection
    {
        return $this->roleUser;
    }

    public function addRoleUser(user $roleUser): self
    {
        if (!$this->roleUser->contains($roleUser)) {
            $this->roleUser[] = $roleUser;
        }

        return $this;
    }

    public function removeRoleUser(user $roleUser): self
    {
        if ($this->roleUser->contains($roleUser)) {
            $this->roleUser->removeElement($roleUser);
        }

        return $this;
    }
}
