<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AddressRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"streetAddress", "numberStreetAddress", "townAddress" },
 *     errorPath="numberStreetAddress",
 *     message="This port is already in use on that host."
 * )
 */
class Address
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Type("string")
     */
    private $streetAddress;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Positive
     */
    private $numberStreetAddress;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Type("string")
     */
    private $townAddress;

    /**
     * @ORM\Column(type="boolean")
     */
    private $activeAddress;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Association", mappedBy="address")
     * @Assert\NotBlank
     */
    private $association;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Country", inversedBy="address")
     * @Assert\NotBlank
     */
    private $country;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Site", mappedBy="address")
     */
    private $site;

    /**
     * @ORM\PrePersist
     */
    public function addAddress() 
    {
        $this->setActiveAddress(true);
    }

    public function __construct()
    {
        $this->association = new ArrayCollection();
        $this->site = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStreetAddress(): ?string
    {
        return $this->streetAddress;
    }

    public function setStreetAddress(string $streetAddress): self
    {
        $this->streetAddress = $streetAddress;

        return $this;
    }

    public function getNumberStreetAddress(): ?string
    {
        return $this->numberStreetAddress;
    }

    public function setNumberStreetAddress(string $numberStreetAddress): self
    {
        $this->numberStreetAddress = $numberStreetAddress;

        return $this;
    }

    public function getTownAddress(): ?string
    {
        return $this->townAddress;
    }

    public function setTownAddress(string $townAddress): self
    {
        $this->townAddress = $townAddress;

        return $this;
    }

    public function getActiveAddress(): ?bool
    {
        return $this->activeAddress;
    }

    public function setActiveAddress(bool $activeAddress): self
    {
        $this->activeAddress = $activeAddress;

        return $this;
    }

    /**
     * @return Collection|Association[]
     */
    public function getAssociation(): Collection
    {
        return $this->association;
    }

    public function addAssociation(Association $association): self
    {
        if (!$this->association->contains($association)) {
            $this->association[] = $association;
            $association->setAddress($this);
        }

        return $this;
    }

    public function removeAssociation(Association $association): self
    {
        if ($this->association->contains($association)) {
            $this->association->removeElement($association);
            // set the owning side to null (unless already changed)
            if ($association->getAddress() === $this) {
                $association->setAddress(null);
            }
        }

        return $this;
    }

    public function getCountry(): ?country
    {
        return $this->country;
    }

    public function setCountry(?country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection|Site[]
     */
    public function getSite(): Collection
    {
        return $this->site;
    }

    public function addSite(Site $site): self
    {
        if (!$this->site->contains($site)) {
            $this->site[] = $site;
            $site->addAddress($this);
        }

        return $this;
    }

    public function removeSite(Site $site): self
    {
        if ($this->site->contains($site)) {
            $this->site->removeElement($site);
            $site->removeAddress($this);
        }

        return $this;
    }
    public function getFullName() {
        return $this->numberStreetAddress . " " . $this->streetAddress . " " . $this->townAddress . " " . $this->country->getNameCountry();
    }
}
