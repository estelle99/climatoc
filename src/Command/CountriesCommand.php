<?php

namespace App\Command;

use App\Service\CountriesService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

Class CountriesCommand extends Command {
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-countries';
    private $service;

    public function __construct(CountriesService $service) {
        $this->service = $service;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Input countries in BDD')
             ->setHelp('This command allows to inport countries in BDD');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Countries Creator',
            '-----------------',
        ]);

        $this->service->setCountriesInBdd($output);
        

        $output->writeln('This is the end');
    }
}

?>