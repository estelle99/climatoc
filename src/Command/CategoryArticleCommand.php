<?php

namespace App\Command;

use App\Service\CategoryArticleService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

Class CategoryArticleCommand extends Command {
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-categories';
    private $service;

    public function __construct(CategoryArticleService $service) {
        $this->service = $service;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Input categories in BDD')
             ->setHelp('This command allows to inport categories in BDD');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Category Creator',
            '-----------------',
        ]);

        $this->service->setCategoryInBdd($output);
        

        $output->writeln('This is the end');
    }
}

?>