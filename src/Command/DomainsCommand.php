<?php

namespace App\Command;

use App\Service\DomainsService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

Class DomainsCommand extends Command {
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-domains';
    private $service;

    public function __construct(DomainsService $service) {
        $this->service = $service;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Input domains in BDD')
             ->setHelp('This command allows to inport domains in BDD');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Domain Creator',
            '-----------------',
        ]);

        $this->service->setDomainInBdd($output);
        

        $output->writeln('This is the end');
    }
}

?>