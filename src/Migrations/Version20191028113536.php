<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191028113536 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE site (id INT AUTO_INCREMENT NOT NULL, name_site VARCHAR(255) NOT NULL, phone_site VARCHAR(255) NOT NULL, email_site VARCHAR(255) NOT NULL, active_site TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE site_address (site_id INT NOT NULL, address_id INT NOT NULL, INDEX IDX_F6EA8D4AF6BD1646 (site_id), INDEX IDX_F6EA8D4AF5B7AF75 (address_id), PRIMARY KEY(site_id, address_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE site_association (site_id INT NOT NULL, association_id INT NOT NULL, INDEX IDX_16AB1E7CF6BD1646 (site_id), INDEX IDX_16AB1E7CEFB9C8A5 (association_id), PRIMARY KEY(site_id, association_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE site_user (site_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_B6096BB0F6BD1646 (site_id), INDEX IDX_B6096BB0A76ED395 (user_id), PRIMARY KEY(site_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE site_address ADD CONSTRAINT FK_F6EA8D4AF6BD1646 FOREIGN KEY (site_id) REFERENCES site (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE site_address ADD CONSTRAINT FK_F6EA8D4AF5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE site_association ADD CONSTRAINT FK_16AB1E7CF6BD1646 FOREIGN KEY (site_id) REFERENCES site (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE site_association ADD CONSTRAINT FK_16AB1E7CEFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE site_user ADD CONSTRAINT FK_B6096BB0F6BD1646 FOREIGN KEY (site_id) REFERENCES site (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE site_user ADD CONSTRAINT FK_B6096BB0A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE site_address DROP FOREIGN KEY FK_F6EA8D4AF6BD1646');
        $this->addSql('ALTER TABLE site_association DROP FOREIGN KEY FK_16AB1E7CF6BD1646');
        $this->addSql('ALTER TABLE site_user DROP FOREIGN KEY FK_B6096BB0F6BD1646');
        $this->addSql('DROP TABLE site');
        $this->addSql('DROP TABLE site_address');
        $this->addSql('DROP TABLE site_association');
        $this->addSql('DROP TABLE site_user');
    }
}
