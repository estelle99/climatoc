<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    // /**
    //  * @return Article[] Returns an array of Article objects
    // */
    
    // public function findByArticleMessage($value)
    // {
    //     $entityManager = require_once join(DIRECTORY_SEPARATOR, [__DIR__, 'bootstrap.php']);

    //     $messageRepo = $entityManager->getRepository(Message::class);
 
    //     $queryBuilder = $entityManager->createQueryBuilder();
        
    //     $queryBuilder->select('u')
    //        ->from(User::class, 'u')
    //        ->where('u.firstname = :firstname')
    //        ->setParameter('firstname', 'First');
        
    //     $query = $queryBuilder->getQuery();
        
    //     echo $query->getDQL(), "\n";
    //     echo $query->getOneOrNullResult();

    // }
    
    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ; 
    }
    */
} 