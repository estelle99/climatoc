<?php

namespace App\Form;

use App\Entity\Domain;
use App\Entity\Address;
use App\Entity\Association;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AssociationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nameAssociation')
            ->add('descriptionAssociation')
            ->add('logoAssociation')
            ->add('linkAssociation')
            ->add('address', EntityType::class, [
                'class' => Address::class,
                'choice_label' => function ($address) {
                    return $address->getNumberStreetAddress()." ".$address->getStreetAddress()." ".$address->getTownAddress().", " .$address->getCountry()->getNameCountry();
                }
            ])
            ->add('domain', EntityType::class, [
                'class' => Domain::class,
                'choice_label' => 'nameDomain'
            ])
            ->add('save', SubmitType::class, ['label' => 'Valider'])        
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Association::class,
        ]);
    }
}
