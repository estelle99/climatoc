<?php

namespace App\Form;

use App\Entity\Contact;
use App\Form\ApplicationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ContactType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mailContact', EmailType::class, $this->getConfiguration("Entrez votre adresse mail", "Adresse mail..."))
            ->add('objectContact', TextType::class, $this->getConfiguration("Entrez objet du message", "Objet..."))
            ->add('textContact', TextType::class, $this->getConfiguration("Entrez votre message", "Message..."))
            ->add('save', SubmitType::class, ['label' => 'Valider'])     
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
