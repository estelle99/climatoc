<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\CategoryArticle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titleArticle')
            ->add('textArticle')
            ->add('picturesArticle') 
            ->add('category', EntityType::class, [
                'class' => CategoryArticle::class,
                'choice_label' => 'themeCategory'
            ])  
            ->add('save', SubmitType::class, ['label' => 'Valider'])               
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
