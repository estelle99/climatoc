<?php

    namespace App\Service;

    use Doctrine\Common\Persistence\ObjectManager;
    use App\Entity\CategoryArticle;
    use Symfony\Component\Console\Output\OutputInterface;
    

    Class CategoryArticleService {

        private $manager;
        /**
         * Fonction construct 
         *
         * @param ObjectManager $manager
         */
        public function __construct(ObjectManager $manager) {
          $this->manager = $manager;
        }

        /**
         * Fonction qui liste tous les catégories d'article et les rentres dans la variable $category
         * Anglais
         * 
         * @return array $category
         */
        function category() {
            $category = array(
              'Agriculture',
              'Climat',
              'Nature',
              'Forêt',
              'Océan',
              'Montagne',
              'Développement agricole durable',
              'Nuisance sonore',
              'Jardin',
              'Pollution'
            );
            // Sort the list.
            natcasesort($category);
            return $category;
        }
        
        /**
         * Fonction qui injecte les catégories en BDD 
         * Anglais
         * 
         * @param OutputInterface $output
         * @return void
         */
        public function setCategoryInBdd(OutputInterface $output) {
            $category = $this->category();
            foreach ($category as $categories) {
              $output->writeln("Domaine traité " . $categories);
              $newCategory = new CategoryArticle();
              $newCategory->setThemeCategory($categories);
                         
              $this->manager->persist($newCategory);
              unset($newCategory);
            }
            $this->manager->flush();
        }
    }

?>