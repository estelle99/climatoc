<?php

    namespace App\Service;

    use Doctrine\Common\Persistence\ObjectManager;
    use App\Entity\Domain;
    use Symfony\Component\Console\Output\OutputInterface;
    

    Class DomainsService {

        private $manager;
        /**
         * Fonction construct 
         *
         * @param ObjectManager $manager
         */
        public function __construct(ObjectManager $manager) {
          $this->manager = $manager;
        }

        /**
         * Fonction qui liste tous les domaines des associations et entre le tout dans $domain
         * Anglais
         * 
         * @return array $domain
         */
        function domain() {
            $domain = array(
              'Agriculture',
              'Climat',
              'Nature',
              'Forêt',
              'Océan',
              'Montagne',
              'Développement agricole durable',
              'Nuisance sonore',
              'Jardin',
              'Pollution'
            );
            // Sort the list.
            natcasesort($domain);
            return $domain;
        }
        
        /**
         * Fonction qui injecte les domains en BDD 
         * Anglais
         * 
         * @param OutputInterface $output
         * @return void
         */
        public function setDomainInBdd(OutputInterface $output) {
            $domain = $this->domain();
            foreach ($domain as $domains) {
              $output->writeln("Domaine traité " . $domains);
              $newDomain = new Domain();
              $newDomain->setNameDomain($domains)
                        ->setActiveDomain(true);
                         
              $this->manager->persist($newDomain);
              unset($newDomain);
            }
            $this->manager->flush();
        }
    }

?>