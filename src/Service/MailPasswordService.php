<?php

    namespace App\Service;

    use Twig\Environment;
    use Doctrine\Common\Persistence\ObjectManager;
    use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    

    Class MailPasswordService {
        private $mail;
        private $renderer;
        private $tokenManager;

        public function __construct(\Swift_Mailer $mail, Environment $renderer, CsrfTokenManagerInterface $tokenManager, ObjectManager $manager) {
            $this->mail = $mail;
            $this->manager = $manager;
            $this->renderer = $renderer;
            $this->tokenManager = $tokenManager;
        }

        public function sendEmail($userEmail) {
            $csrfToken = $this->tokenManager
                ? $this->tokenManager->getToken('authenticate')->getValue()
                : null;
            // SELECT
                $email = $userEmail[0]->getEmail();
            // INSERT
                $userEmail[0]->setTokenUser($csrfToken);
            
            $this->manager->persist($userEmail[0]);
            $this->manager->flush();

            $message = (new \Swift_Message('Mot de passe oublié'))
                ->setFrom('estelle.sizaire99@gmail.com')
                ->setTo($email)
                ->setBody($this->renderer->render(
                    'email/emailForgotPassword.html.twig', [
                        'token' => $csrfToken
                    ]
                ), 'text/html'
            );
            $this->mail->send($message);
        }
    }

?>