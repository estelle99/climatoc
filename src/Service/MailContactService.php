<?php

    namespace App\Service;

    use Twig\Environment;
    use Symfony\Component\Console\Output\OutputInterface;
    

    Class MailContactService {
        private $mail;
        private $renderer;

        public function __construct(\Swift_Mailer $mail, Environment $renderer) {
            $this->mail = $mail;
            $this->renderer = $renderer;
        }

        public function sendEmail($contact) {
            // SELECT 
            $email = $contact->getMailContact();
            $text = $contact->getTextContact();
            $object = $contact->getObjectContact();

            $message = (new \Swift_Message('Formulaire de contact'))
                ->setFrom($email)
                ->setTo('estelle.sizairepro@gmail.com')
                ->setBody($this->renderer->render(
                    'email/emailContact.html.twig', [
                        'object' => $object,
                        'text' => $text
                    ]
                ), 'text/html'
            );
            $this->mail->send($message);
        }
    }

?>