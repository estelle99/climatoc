<?php

namespace App\DataFixtures;

use App\Entity\Role;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder) {
        $this->passwordEncoder = $passwordEncoder;
    }
    
    public function load(ObjectManager $manager)
    {
        // adding roles admin and user
        $adminRole = new Role();
        $adminRole->setTypeRole("ROLE_ADMIN")
                  ->setActiveRole(true);
        $manager->persist($adminRole);

        $adminUser = new User();
        $adminUser->setUsername('admin')
                 ->setEmail('admin.admin@gmail.com')
                 ->setPassword($this->passwordEncoder->encodePassword($adminUser, 'admin' ))
                 ->setIsActive(true)
                 ->addUserRole($adminRole);
        $manager->persist($adminUser);
        $manager->flush();
    }
}
